# test-utils #

Extends the junit functionality.

### Features ###

* priority for methods (like testng)
* methods dependency (like testng)

### Deployment commands ###

* mvn release:clean release:prepare -Possrh
* mvn release:perform -Possrh
* goto https://oss.sonatype.org/#stagingRepositories, search the project, close & release