package com.tatateo.test.tests;

import com.tatateo.test.Conditions;
import com.tatateo.test.DependencyRunner;
import org.junit.*;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

/**
 * @author teo
 * @since 24.01.16
 */
@RunWith(DependencyRunner.class)
public class JunitRules {
    static final java.util.logging.Logger L = java.util.logging.Logger.getLogger(DependencyRunner.class.getName());

    @Rule
    public TestName name = new TestName();

    static final List<String> order = new ArrayList<String>();

    @AfterClass
    public static void afterClass() {
        L.info("Final order: " + order);
        Assert.assertThat(order, contains("test3", "test2", "test4", "test1"));
    }

    @Test
    @Conditions(dependsOn = {"test2", "test4"})
    public void test1() {
        L.info("Running [" + name.getMethodName() + "]");
        Assume.assumeThat(order, hasItems("test2", "test4"));
        order.add(name.getMethodName());
    }

    @Test
    @Conditions(dependsOn = "test3")
    public void test2() {
        L.info("Running [" + name.getMethodName() + "]");
        Assume.assumeThat(order, hasItems("test3"));
        order.add(name.getMethodName());
    }

    @Test
    @Conditions(priority = 1,dependsOn = "test2")
    public void test4() {
        L.info("Running [" + name.getMethodName() + "]");
        Assume.assumeThat(order, hasItems("test2"));
        order.add(name.getMethodName());
    }

    @Test
    @Conditions()
    public void testFail() {
        L.info("Running [" + name.getMethodName() + "]");
        Assert.fail();
    }

    @Test
    public void test3() {
        L.info("Running [" + name.getMethodName() + "]");
        Assume.assumeThat(order, empty());
        order.add(name.getMethodName());
    }

    @Test
    @Conditions(dependsOn = "testFail")
    public void testWithFailedDep() {
        L.info("Running [" + name.getMethodName() + "]");
    }

    @Rule
    public ExternalResource externalResource = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            //executed before each test
            L.info("before - order=" + order);
        }

        @Override
        protected void after() {
            //executed after each test
            L.info("after - order=" + order);
        }
    };
}
