package com.tatateo.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies a test method's meta information
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Conditions {

    /**
     * list of test methods that should run before this test method.
     */
    String[] dependsOn() default {};

    /**
     * priority of the test method
     */
    int priority() default 0;
}
