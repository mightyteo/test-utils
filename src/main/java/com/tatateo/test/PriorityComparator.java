package com.tatateo.test;

import java.util.Comparator;

/**
 * Reversed priority comparator.
 * @author teo
 * @since 14.02.2016
 */
public class PriorityComparator implements Comparator<ExtendedMethod> {
    public int compare(ExtendedMethod left, ExtendedMethod right) {
        return Integer.valueOf(right.priority).compareTo(left.priority);
    }
}
